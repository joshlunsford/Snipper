﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snipper
{
    public partial class SelectRegion : Form
    {

        public Rectangle mRect;

        public SelectRegion()
        {
            InitializeComponent();
            this.Opacity = 0.50;
            this.DoubleBuffered = true;
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            
        }

        private void SelectRegion_Load(object sender, EventArgs e)
        {

        }


        //Initiate rectangle with mouse down event
        protected override void OnMouseDown(MouseEventArgs e)
        {
            mRect = new Rectangle(e.X, e.Y, 0, 0);
            this.Invalidate();
        }

        //check if mouse is down and being draged, then draw rectangle
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mRect = new Rectangle(mRect.Left, mRect.Top, e.X - mRect.Left, e.Y - mRect.Top);
                this.Invalidate();
            }
        }
        //Mouse up get coords
        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Close();
                
            }
        }

        //draw the rectangle on paint event
        protected override void OnPaint(PaintEventArgs e)
        {
            //Draw a rectangle with 2pixel wide line
            using (Pen pen = new Pen(Color.Red, 2))
            {
                e.Graphics.DrawRectangle(pen, mRect);
            }

        }
    }
}
