﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snipper
{
    public partial class Form1 : Form
    {
        Rectangle rect;
        Bitmap bmp;
        Graphics g;
        SelectRegion sr;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }





        private void takeScreenshot() {
            //rect = new Rectangle(500, 0, 1000, 1000);
            bmp = new Bitmap(rect.Width, rect.Height, PixelFormat.Format32bppArgb);
            g = Graphics.FromImage(bmp);
            g.CopyFromScreen(rect.Left, rect.Top, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);
            //bmp.Save(fileName, ImageFormat.Jpeg);
            Clipboard.SetImage(bmp);
            pictureBox1.Image = bmp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            takeScreenshot();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            sr = new SelectRegion();
            sr.Show();
            sr.FormClosing += Sr_FormClosing;
        }

        private void Sr_FormClosing(object sender, FormClosingEventArgs e)
        {
            button1.Enabled = true;
            rect = sr.mRect;
            takeScreenshot();
        }
    }
}
