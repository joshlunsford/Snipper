# Snipper

Snipper allows you to take a screenshot of a predifined area of your screen as many times as you want with the push of a button

## Getting Started

Download, Install, then run.


## Authors

* **Josh Lunsford** - *Initial work* - [Josh Lunsford](https://gitlab.com/joshlunsford)

See also the list of [contributors](https://gitlab.com/joshlunsford/snipper/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

